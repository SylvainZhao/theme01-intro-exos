;; # Exercice 3 : Phrase-o-matic

(ns theme01-clojure.ex03-phrases)


;; => #'theme01-clojure.ex03-phrases/ex03-phrases

(defn gen-article
  "Générateur d'article."
  []
  (rand-nth ["le" "la" "un" "une"]))

;; => #'theme01-clojure.ex03-phrases/gen-article
(repeatedly 10 gen-article)

;; => ("une" "un" "une" "un" "la" "la" "un" "la" "une" "la")
;; ## Question 1 : générateurs d'adjectifs et de noms
(defn gen-adjectif
  "Générateur d'adjectif."
  []
  (rand-nth ["beau" "haut" "grand" "petit"]))
(defn gen-nom
  "Générateur de nom."
  []
  (rand-nth ["Sylvain" "Leo" "Adam" "Ben" "Cecile"]))
;; ## Question 2 : générateur de proposition nominale
(defn gen-prop-nominale
  "Générateur de prop ."
  []
  (str(gen-article) " "
(gen-adjectif) " "
(gen-nom)))
;; ## Question 3 : générateur de proposition verbale
(defn gen-verbe
  "Générateur de verbe."
  []
  (rand-nth ["est" "etait" "parle avec"]))

(defn gen-prop-verbale
  "Générateur de prop-verbale ."
  []
  (str (gen-verbe) " "
       (gen-prop-nominale)))
;; ## Question 4 : générateur de phases
(defn gen-phrase
  "Générateur de phrase."
  []
  (str (gen-prop-nominale) " " (gen-prop-verbale)))
;; ## Question 5 : phrase-o-matic++


