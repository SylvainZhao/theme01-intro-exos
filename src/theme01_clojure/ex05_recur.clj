;; # Exercice 5 : Récursion et collections

(ns theme01-clojure.ex05-recur
  (:use midje.sweet))



;; ## Question 1 : Construction d'un vecteur

(declare inter)

(defn inter [n m k]
  "construit l'intervalle [n:m] par pas de k dans un vecteur"
  (loop [i n, vres []]
    (if (<= i m)
      (recur (+ i k) (conj vres i))
      vres)))
(fact "`inter` construit les bons intervalles."
      (inter 2 5 1) => [2 3 4 5]
      (inter 2 5 2) => [2 4]
      (inter 2 2 10) => [2]
      (inter 3 2 1) => []
      (inter 10 50 10) => [10 20 30 40 50])

;; ## Question 2 : Transformation d'un vecteur

(declare vmap)

(defn vmap [op v]
  (loop [i 0, res []]
    (if (< i (count v))
      (recur (inc i) (conj res (op (get v i))))
      res)))



(fact "`vmap` applique bien la fonction au vecteur."
      (vmap - [1 2 3 4 5]) => [-1 -2 -3 -4 -5]
      (vmap (fn [x] (* x x)) [1 2 3 4 5]) => [1 4 9 16 25]
      (vmap #(count %) [[1 2] ["a" "b" "c"] []]) => [2 3 0])

;; ## Question 3 : Réductions d'un vecteur


(declare vmax)
(defn vmax [v]
  (loop [s (rest v), mx (first v)]
      (if (seq s)
        (if (pos? (compare (first s) mx))
          (recur (rest s) (first s))
          (recur (rest s) mx))
    	mx)))
(declare vmax-nth)
(defn vmax-nth
  [v]
  (loop [i 1, mx (nth v 0), imax 0]
    (if (< i (count v))
      (let [xi (nth v i)]
        (if (pos? (compare xi mx))
          (recur (inc i) xi i)
          (recur (inc i) mx imax)))
      imax)))





(fact "`vmax` retourne bien l'élément maximal, et `vmax-nth` son indice."
      (vmax [2 3 5 1 2 0]) => 5
      (vmax-nth [2 3 5 1 2 0]) => 2
      (vmax []) => nil
      (vmax-nth []) => nil
      (vmax ["un" "deux" "trois" "quatre" "cinq" "six"]) => "un"
      (vmax-nth ["un" "deux" "trois" "quatre" "cinq" "six"]) => 0
      (vmax-nth [2 9 2 9]) => 1)

;; ## Question 4 : Construction d'une map

(declare mkmap)
(defn mkmap [v]
  (loop [v v, m {}]
    (if (seq v)
      (recur (rest v) (assoc m (ffirst v) (second (first v))))
      m)))


(fact "`mkmap` construit la bonne map."
      (mkmap [[:un, 1] [:deux, 2] [:trois, 3]])
      => {:deux 2, :un 1, :trois 3}
      (mkmap []) => {})

;; ## Question 5 : Réduction d'une map

(declare mmaxv)
(defn mmaxv [m]
  (loop [m m, mx (second (first m)), res nil]
    (if (seq m)
      (let [v (second (first m)) k (ffirst m)]
        (if (> v mx)
          (recur (rest m) v (ffirst m))
          (recur (rest m) mx res)
          ))
      res)))

(fact "`mmaxv` retourne la bonne clé."
      (mmaxv {:a 1, :b 3, :c 2, :d -1}) => :b
      (mmaxv {}) => nil)

;; ## Question 6 : construction d'un ensemble

(declare premiers)
;;loop starts from i = 1, j = 3
;;by using an anoymous function isPremier[j] to tell is j a premier ot not
;;if j is a premier, then add j into map, (inc i), (inc j)
;;if j in not a premier, (inc j)
;;if (= i n), return the map
(defn premiers [n]
  (if (= 1 n)
    #{2}
    (loop [m #{2}, i 1, j 3]
      (if (< i n)
        (if ((fn [n]
               (loop [num (range 2 n)]
                 (if (seq num)
                   (if (not (zero? (mod n (first num))))
                     (recur (rest num))
                     false)
                   true))) j)
          (recur (conj m j) (inc i) (inc j))
          (recur m i (inc j)))
        m))))

(fn isPremier [n]
  (loop [num (range 2 n)]
    (if (seq num)
      (if (not (zero? (mod n (first num))))
        (recur (rest num))
        false)
      true)))
      

(fact "`premiers` retourne bien les `n` premiers premiers"
      (premiers 1) => #{2}
      (premiers 10) => #{2 3 5 29 7 11 13 17 19 23}
      (count (premiers 10)) => 10
      (last (premiers 100)) => 379)

;; ## Question 7 : Opérations ensemblistes.

(declare union)
(declare intersection)
(declare difference)

(defn union [m1 m2]
  (loop [res m1, m2 m2]
    (if (seq m2)
      (recur (conj res (first m2)) (rest m2))
      res)))

(defn intersection [m1 m2]
  (loop [res #{}, m1 m1, m2 m2]
    (if (< (count m1) (count m2))
      (if (seq m1)
        (if (contains? m2 (first m1))
          (recur (conj res (first m1)) (rest m1) m2)
          (recur res (rest m1) m2))
        res)
      (if (seq m2)
        (if (contains? m1 (first m2))
          (recur (conj res (first m2)) (rest m2) m1)
          (recur res (rest m2) m1))
        res))))

(defn difference [m1 m2]
  (loop [res #{}, m1 m1]
    (if (seq m1)
      (if (not (contains? m2 (first m1)))
        (recur (conj res (first m1)) (rest m1))
        (recur res (rest m1)))
      res)))

(facts "sur les opérations ensemblistes."
       (union #{1 2 4} #{2 3 4 5}) => #{1 2 3 4 5}
       (intersection #{1 2 4} #{2 3 4 5}) => #{2 4}
       (difference #{2 3 4 5} #{1 2 4}) => #{3 5}
       (union #{1 2 3} #{}) => #{1 2 3}
       (union #{} #{1 2 3}) => #{1 2 3}
       (intersection #{1 2 3} #{}) => #{}
       (intersection #{} #{1 2 3}) => #{}
       (difference #{1 2 3} #{}) => #{1 2 3}
       (difference #{} #{1 2 3}) => #{})




