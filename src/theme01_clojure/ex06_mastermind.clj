;; # Exercice 6 : Mastermind

(ns theme01-clojure.ex06-mastermind
  (:use midje.sweet))



;; ## Question 1 : tirage du code secret

(declare code-secret)
(defn code-secret [n]
  (let [secret [:rouge :bleu :vert :jaune :noir :blanc]]
    (loop [n n, res #{}]
      (if (> n 0)
        (recur (dec n) (conj res (rand-nth secret)))
        res))))

(fact "Le `code-secret` est bien composé de couleurs."
      (every? #{:rouge :bleu :vert :jaune :noir :blanc} 
              (code-secret 4))
      => true)

(fact "Le `code-secret` a l'air aléatoire."
      (> (count (filter true? (map not= 
                                   (repeatedly 20 #(code-secret 4))
                                   (repeatedly 20 #(code-secret 4)))))
         0)
      => true)


;; ## Question 2 : indications

(declare indications)
(defn indications
  [code tenta]
  (loop [i 0, res []]
    (if (< i (count code))
      (if (some #(= (nth tenta i) %) code)
        (if (= (nth code i) (nth tenta i))
          (recur (inc i) (conj res :good))
          (recur (inc i) (conj res :color)))
        (recur (inc i) (conj res :bad)))
      res)))

(fact "`indications` sont les bonnes."
      (indications [:rouge :rouge :vert :bleu] 
                   [:vert :rouge :bleu :jaune])
      => [:color :good :color :bad]
      
      (indications [:rouge :rouge :vert :bleu] 
                   [:bleu :rouge :vert :jaune])
      => [:color :good :good :bad]
      
      (indications [:rouge :rouge :vert :bleu] 
                   [:rouge :rouge :vert :bleu])
      => [:good :good :good :good]
      
      (indications [:rouge :rouge :vert :vert] 
                   [:vert :bleu :rouge :jaune])
      => [:color :bad :color :bad])

;; ## Question 3 : fréquences

(declare frequences)
(defn frequences
  [v]
  (reduce #(assoc %1 %2 (inc (%1 %2 0))) {} v))

(fact "les `frequences` suivantes sont correctes."
      (frequences [:rouge :rouge :vert :bleu :vert :rouge])
      => {:rouge 3 :vert 2 :bleu 1}
      
      (frequences [:rouge :vert :bleu])
      => {:rouge 1 :vert 1 :bleu 1}
      
      (frequences [1 2 3 2 1 4]) => {1 2, 2 2, 3 1, 4 1})

;; ## Question 4 : fréquences disponibles

(declare freqs-dispo)
(defn freqs-dispo
  [code tenta]
  (let [freq (frequences code) res (reduce #(assoc %1 %2 0) freq code)]
    (loop [i 0, res res]
      (if (< i (count code))
        (if (not= (nth tenta i) :good)
          (recur (inc i) (assoc res (nth code i) (inc (res (nth code i)))))
          (recur (inc i) res))
        res))))

(fact "Les fréquences disponibles de `freqs-dispo` sont correctes."
      (freqs-dispo [:rouge :rouge :bleu :vert :rouge]
                   [:good :color :bad :good :color])
      => {:bleu 1, :rouge 2, :vert 0})

;; ## Question 5 : filtrer par cadinalité (+ difficile)

(declare filtre-indications)
(defn filtre-indications
  [code tenta indi]
  (let [freq (frequences code) freq_not_bad (reduce #(assoc %1 %2 0) freq code)]
    (loop [i 0, freq_not_bad freq_not_bad, indi indi]
      (if (< i (count code))
        (if (not= (nth indi i) :bad)
          (recur (inc i) (assoc freq_not_bad (nth tenta i) (inc (freq_not_bad (nth tenta i)))) (if (= (freq_not_bad (nth tenta i)) (freq (nth tenta i)))
                                                                                                 (assoc indi i :bad)
                                                                                                 indi))
          (recur (inc i) freq_not_bad indi))
        indi))))

(fact "Le `filtre-indications` fonctionne bien."
      (filtre-indications [:rouge :rouge :vert :bleu] 
                          [:vert :rouge :bleu :jaune]
                          [:color :good :color :bad])
      => [:color :good :color :bad]
      
      (filtre-indications [:rouge :vert :rouge :bleu] 
                          [:rouge :rouge :bleu :rouge]
                          [:good :color :color :color])
      => [:good :color :color :bad])
      

;; ## Questions subsidiaire


